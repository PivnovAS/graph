#include <iostream>
#include <ctime>
#include <header.h>


int main()
{
    Graph<int>A(6); //Создание графа с 5 вершинами

    //A.setValueVertexRandom(); //Рандомная инициализация вершин

    A.setVertex(1, 65);
    A.setVertex(2, 52);
    A.setVertex(3, 73);
    A.setVertex(4, 54);
    A.setVertex(5, 43);
    A.setVertex(6, 45);

    A.showVertex(); //Вывод на экран значений вершин
    cout << endl;

    //A.setValueVertex(); //Задать вершины с клавиатура (в случае использования можно отключить методы setValueVertexRandom и showVertex)

    //A.addConnectionAllListAdjacency(); //Инициализация матрицы смежности через списки смежности

    //A.delConnectionListAdjacency(1); //Удаление связи в первой строке

    cout << endl;

    A.showAdjacencyMatrix(); //Вывод на экран матрицы смежности

    cout << endl;

    A.addVertex(5); //Добавление вершины

    A.addConnectionAllListAdjacency(); //Инициализация матрицы смежности через списки смежности
    cout << endl;

    A.delVertex(7);

    A.showVertex(); //Вывод на экран значений вершин
    cout << endl;

    //A.directedToUndirected();

    A.showAdjacencyMatrix(); //Вывод на экран матрицы смежности

    cout << endl;
    cout << A.directedGraph() << endl; // Проверка на ориентированность. 0 - неориентированный, 1 ориентированный

    cout << A.existencePath(4, 2) << endl; // Проверка существует ли путь

    cout << A.connectivityGraph(); // Проверка на связность. //Связность графа 1 - нет связи; 2 - слаба-связный; 3 - сильно-связный;

    cout << endl;

    cout << A.getVertex(1) << endl;
    cout << A.getVertex(2) << endl;
    cout << A.getVertex(3) << endl;
    cout << A.getVertex(4) << endl;
    cout << A.getVertex(5) << endl;
    cout << A.getVertex(6) << endl;

    A.clear();

    return 0;
}
