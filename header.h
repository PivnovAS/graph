#ifndef HEADER_H
#define HEADER_H

#include<iostream>
#include<QString>
#include<vector>
using namespace std;

template <class T>
class Graph
{    
    int SizeVertex;
    T* Vertex;
    int** AdjacencyMatrix;
    
public:

    Graph(int sizeV); //конструктор
 
    T &operator[] (int index); //Перегрузка []

    bool directedGraph(); //Проверка на ориентированность графа. true - ориентированный, false - неориентированный

    int connectivityGraph(); //Связность графа 1 - нет связи; 2 - слабо-связный; 3 - сильно-связный;

    bool existencePath(int Vertex1, int Vertex2); // Существует ли путь между двумя вершинами. true - да, false - нет

    int sizeVertex(); //Получение колличества вершин

    void directedToUndirected(); //Превращение ориентированного графа в неориентированный

    void setValueVertex(); //Задать значения вершин с клавиатуры

    void setValueVertexRandom(); //Задать значения вершин рандомно

    void showVertex(); //Вывод на экран значений вершин

    void showAdjacencyMatrix(); //Вывод на экран матрицы смежности

    void addVertex(T value); //Добавить вершину

    void addConnectionListAdjacency(int index); //Добавление связей через список одной вершины

    void addConnectionAllListAdjacency(); //Добавление связей через список всех вершин

    void delConnectionListAdjacency(int index); //Удаление связей через список одной вершины

    void clearConnection(); //Удаление связей (иначе говоря очистка матрицы смежности)

    void clearVertex(); //Обнуление значений вершин

    void clear(); //Удаление связей и обнуление значений вершин

    void delVertex(int index); //Удаление вершины и связей с этой вершиной

    void setVertex(int index, T value);

    T getVertex(int index);
};

template <class T>
Graph<T>::Graph(int sizeV)
{
    SizeVertex = sizeV;
    Vertex = new T[SizeVertex];

    AdjacencyMatrix = new int*[SizeVertex];
    for (int i = 0; i<SizeVertex; i++)
    {
        AdjacencyMatrix[i] = new int[SizeVertex];
    }

    for(int i = 0; i<SizeVertex; i++)
        for(int j = 0; j<SizeVertex; j++)
        {
            AdjacencyMatrix[i][j] = 0;
        }
}

template <class T>
T &Graph<T>::operator [](int index)
{
    if(index>=0 && index<SizeVertex)
    {
        return Vertex[index];
    }
    else
        throw "Out of graph's element.";
}

template <class T>
int Graph<T>::sizeVertex()
{
    return SizeVertex;
}

template <class T>
void Graph<T>::setValueVertex()
{
    for (int i = 0; i<sizeVertex(); i++)
    {
        cout << "Vertex " << i+1 << ": ";
        cin >> Vertex[i];
    }
}

template <class T>
void Graph<T>::setValueVertexRandom()
{
    for (int i = 0; i<sizeVertex(); i++)
    {
        Vertex[i] = rand()%10;
    }
}

template <class T>
void Graph<T>::showVertex()
{
    for(int i=0; i<SizeVertex; i++)
    {
        cout << "Vertex " << i+1 << ": " << Vertex[i] << endl;
    }
}

template <class T>
void Graph<T>::showAdjacencyMatrix()
{
    for(int i = 0; i<SizeVertex; i++)
    {
        for(int j = 0; j<SizeVertex; j++)
        {
            cout << AdjacencyMatrix[i][j] << " ";
        }
        cout << endl;
    }
}

template <class T>
void Graph<T>::addVertex(T value)
{
    //Добавление вершины
    T* buffer = new T[SizeVertex];
    for(int i=0; i<SizeVertex; i++)
    {
        buffer[i]=Vertex[i];
    }
    delete[] Vertex;
    Vertex = new T[SizeVertex + 1];
    for(int i=0; i<SizeVertex; i++)
    {
        Vertex[i]=buffer[i];
    }
    Vertex[SizeVertex] = value;
    SizeVertex++;

    //Добавление строки и столбца в матрицу смежности
    int** bufferMatrix = new int*[SizeVertex];
    for (int i = 0; i<SizeVertex; i++)
    {
        bufferMatrix[i] = new int[SizeVertex];
    }

    for(int i = 0; i<SizeVertex - 1; i++)
        for(int j = 0; j<SizeVertex - 1; j++)
        {
            bufferMatrix[i][j] = AdjacencyMatrix[i][j];
        }

    delete [] AdjacencyMatrix;

    AdjacencyMatrix = new int*[SizeVertex];
    for (int i = 0; i<SizeVertex; i++)
    {
        AdjacencyMatrix[i] = new int[SizeVertex];
    }

    for(int i = 0; i<SizeVertex - 1; i++)
        for(int j = 0; j<SizeVertex - 1; j++)
        {
            AdjacencyMatrix[i][j] = bufferMatrix[i][j];
        }

    for (int i = 0; i<SizeVertex; i++)
    {
        AdjacencyMatrix[SizeVertex-1][i] = 0;
        AdjacencyMatrix[i][SizeVertex-1] = 0;
    }
}

template <class T>
void Graph<T>::addConnectionListAdjacency(int index)
{
    if (index > 0 && index <= SizeVertex)
    {
        char ch = '0';

        cout << "Connection with " << index << " vertex: ";
        while (ch != '\n')
        {
            int index2;
            cin >> index2;
            if ((index2>0) && (index2<=SizeVertex))
            {
                AdjacencyMatrix[index-1][index2-1] = 1;
            }
            ch = cin.get();
            if (ch == '\n')
               break;
        }
    }
}

template <class T>
void Graph<T>::addConnectionAllListAdjacency()
{
    for(int i = 1; i<=SizeVertex; i++)
    {
        addConnectionListAdjacency(i);
    }
}

template <class T>
void Graph<T>::delConnectionListAdjacency(int index)
{
    if (index > 0 && index <= SizeVertex)
    {
        char ch = '0';

        cout << "Delete connection with " << index << " vertex: ";
        while (ch != '\n')
        {
            int index2;
            cin >> index2;
            if ((index2>0) && (index2<=SizeVertex))
            {
                AdjacencyMatrix[index-1][index2-1] = 0;
            }
            ch = cin.get();
            if (ch == '\n')
               break;
        }
    }

}

template <class T>
void Graph<T>::clearConnection()
{
    for(int i = 0; i<SizeVertex; i++)
        for (int j = 0; j<SizeVertex; j++)
        {
            AdjacencyMatrix[i][j] = 0;
        }
}

template <class T>
void Graph<T>::clearVertex()
{
    for(int i = 0; i<SizeVertex; i++)
        Vertex[i] = 0;
}

template <class T>
void Graph<T>::clear()
{
    clearConnection();
    clearVertex();
}

template<class T>
void Graph<T>::delVertex(int index)
{
    //Удаление вершины
    index--;
    T* bufferVertex = new T[SizeVertex-1];
    for(int i = 0; i<index; i++)
    {
        bufferVertex[i] = Vertex[i];
    }

    for(int i = index+1; i<SizeVertex; i++)
    {
        bufferVertex[i-1] = Vertex[i];
    }
    delete[] Vertex;
    Vertex = new T[SizeVertex-1];

    SizeVertex--;

    for(int i = 0; i<SizeVertex; i++)
    {
        Vertex[i] = bufferVertex[i];
    }

    delete[] bufferVertex;

    //Удаление строки и столбца из матрицы смежности
    int** bufferMatrix = new int*[SizeVertex];
    for(int i = 0; i<SizeVertex; i++)
    {
        bufferMatrix[i] = new int[SizeVertex];
    }

    for(int i = 0; i<index; i++)
        for(int j = 0; j<index; j++)
        {
            bufferMatrix[i][j] = AdjacencyMatrix[i][j];
        }

    for(int i = index; i<SizeVertex; i++)
        for(int j = 0; j<index; j++)
        {
            bufferMatrix[i][j] = AdjacencyMatrix[i+1][j];
        }

    for(int i = 0; i<index; i++)
        for(int j = index; j<SizeVertex; j++)
        {
            bufferMatrix[i][j] = AdjacencyMatrix[i][j+1];
        }

    for(int i = index; i<SizeVertex; i++)
        for(int j = index; j<SizeVertex; j++)
        {
            bufferMatrix[i][j] = AdjacencyMatrix[i+1][j+1];
        }

    delete[] AdjacencyMatrix;

    AdjacencyMatrix = new int*[SizeVertex];
    for (int i = 0; i<SizeVertex; i++)
    {
        AdjacencyMatrix[i] = new int[SizeVertex];
    }

    for(int i = 0; i<SizeVertex; i++)
        for(int j = 0; j<SizeVertex; j++)
        {
            AdjacencyMatrix[i][j] = bufferMatrix[i][j];
        }
}

template <class T>
bool Graph<T>::directedGraph()
{
    int counter = 0;
    for(int i = 0; i<SizeVertex; i++)
        for(int j = 0; j<SizeVertex; j++)
        {
            if(AdjacencyMatrix[i][j] == AdjacencyMatrix[j][i])
                counter++;
        }
    if (counter == SizeVertex*SizeVertex)
        return false;
    else return true;
}

template <class T>
bool Graph<T>::existencePath(int Vertex1, int Vertex2)
{
    Vertex1--;
    Vertex2--;
    if(Vertex1 < SizeVertex && Vertex1>=0 && Vertex2 < SizeVertex && Vertex2>=0)
    {
        // Копирование матрицы смежности, которую можно было бы в последствии изменять
        int** AdjacencyMatrixForThisFoo = new int*[SizeVertex];
        for(int i = 0; i<SizeVertex; i++)
        {
            AdjacencyMatrixForThisFoo[i] = new int[SizeVertex];
        }

        for (int i = 0; i<SizeVertex; i++)
            for (int j = 0; j<SizeVertex; j++)
            {
                AdjacencyMatrixForThisFoo[i][j] = AdjacencyMatrix[i][j];
            }

        // Поиск пути
        bool end = false;
        int i = Vertex1;
        int j = 0;
        int CounterMotion = 1;

        vector <int> save(1);
        save[0] = Vertex1;

        while (end == false)
        {
            if (AdjacencyMatrixForThisFoo[i][j] == 1)
            {
                AdjacencyMatrixForThisFoo[i][j] = 0;
                save.push_back(i);
                i = j;
                j = 0;
                CounterMotion++;
                if (i == Vertex2)
                {
                    return true;
                    end = true;
                }
            }

            else if (AdjacencyMatrixForThisFoo[i][j] == 0)
            {
                j++;
                if (j == SizeVertex)
                {
                    CounterMotion--;
                    if (CounterMotion != 0)
                    {
                        i = save[CounterMotion];
                        j = 0;
                    }

                    else if (CounterMotion == 0)
                    {
                        return false;
                        end = true;
                    }
                }
            }
        }
        delete [] AdjacencyMatrixForThisFoo;
    }

    else
        throw "Nonexistent vertices";

    return 0;
}

template <class T>
void Graph<T>::directedToUndirected()
{
    for (int i = 0; i<SizeVertex; i++)
        for(int j = 0; j<SizeVertex; j++)
        {
            if (AdjacencyMatrix[i][j] == 0)
            AdjacencyMatrix[i][j] = AdjacencyMatrix[j][i];
        }
}

template <class T>
int Graph<T>::connectivityGraph()
{
    bool end = false;
    for (int i = 0; i<SizeVertex; i++)
    {
        for (int j = 0; j<SizeVertex; j++)
        {
            if (existencePath(i+1, j+1) == false)
            {
                int** bufferMatrix = new int*[SizeVertex];
                for(int i2 = 0; i2<SizeVertex; i2++)
                {
                    bufferMatrix[i2] = new int[SizeVertex];
                }

                for (int i2 = 0; i2<SizeVertex; i2++)
                    for (int j2 = 0; j2<SizeVertex; j2++)
                    {
                        bufferMatrix[i2][j2] = AdjacencyMatrix[i2][j2];
                    }

                directedToUndirected();
                for (int i2 = 0; i2<SizeVertex; i2++)
                {
                    for (int j2 = 0; j2<SizeVertex; j2++)
                    {
                        if (existencePath(i2+1, j2+1) == false)
                        {
                            return 1; //diconected
                            end = true;

                            for (int i3 = 0; i3<SizeVertex; i3++)
                                for (int j3 = 0; j3<SizeVertex; j3++)
                                {
                                    AdjacencyMatrix[i3][j3] = bufferMatrix[i3][j3];
                                }

                            break;
                        }
                        else return 2; // weakly connected
                    }
                    if (end == true)
                    {
                        break;
                    }
                }
                end = true;
                delete [] bufferMatrix;
                break;
            }
            else return 3; //strongly connected
        }
        if (end == true)
        {
            break;
        }
    }

    return 0;
}

template <class T>
void Graph<T>::setVertex(int index, T value)
{
    Vertex[index-1] = value;
}

template <class T>
T Graph<T>::getVertex(int index)
{
    return Vertex[index-1];
}

#endif // HEADER_H
